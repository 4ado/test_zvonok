1. Задача:\
Даны две таблицы в PostgresSQL - таблица статей и таблица комментариев к этим статьям. Необходимо написать запрос, который выведет все статьи без комментариев (у которых нет комментариев).\
Решение:
```sql 
SELECT id FROM article WHERE NOT EXISTS (SELECT 1 FROM comment WHERE comment.article_id = article.id LIMIT 1);
```
2. Задача:\
Необходимо написать программу на python, которая выводит статистику по каждому работнику + сумму часов, например:\
Решение:
```python
def process_data(data_string: str) -> None:
    data_rows = data_string.split('\n')
    result = {}
    for data_row in data_rows:
        words = data_row.split()
        name, hours = str(' '.join(words[:-1])), words[-1]
        if name in result:
            result[name].append(hours)
        else:
            result[name] = [hours]
    print("\n".join([f"{name}: {', '.join(hours)}; sum: {sum(map(int, hours))}" for name, hours in result.items()]))
```